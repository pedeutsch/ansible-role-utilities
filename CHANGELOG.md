# Changelog


## [2.0.1](https://gitlab.com/sjugge/ansible/ansible-role-utilities/tags/2.0.1)

* #10 fix for upgrade on RHEL.

## [2.0.0](https://gitlab.com/sjugge/ansible/ansible-role-utilities/tags/2.0.0)

* #1 & #5 Molecule has been integrated, CI has been setup.

## [1.2.0](https://gitlab.com/sjugge/ansible/ansible-role-utilities/tags/1.2.0)

* #2 Added cron weekday support.

## [1.1.0](https://gitlab.com/sjugge/ansible/ansible-role-utilities/tags/1.1.0)

* Added tags to tasks.

## [1.0.0](https://gitlab.com/sjugge/ansible/ansible-role-utilities/tags/1.0.0)

**breaking changes**:

*  `util_user` is replaced by `util_user_group` and `util_user_host`
*  `util_key` is replaced by `util_keyr_group` and `util_key_host`

Users and keys are now manageable through `*_group` and `*_host` variables.

The idea being that users can be defined on `group_var` and `host_var` level with one overwriting the other.
This allows specific users or keys to be added by default to a group and more fine-grained control per host when a specific user needs access to a single or only a select few hosts.

## [0.1.0](https://gitlab.com/sjugge/ansible/ansible-role-utilities/tags/0.1.0/)

Initial release
