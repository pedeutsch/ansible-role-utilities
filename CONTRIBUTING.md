# Contributing

## Reporting issues

* [Open an issue](https://gitlab.com/sjugge/ansible-role-utilities/issues/new)
* Select one of the templates and complete the requested information.

## Merge requests

* [Create a merge request](https://gitlab.com/sjugge/ansible-role-utilities/merge_requests/new)
* Select a relevant template and complete the requested information.

## Development

Clone the repo to `sjugge.utilities`:

```bash
git clone git@gitlab.com:sjugge/ansible-role-utilities.git sjugge.utilities
```

Ensure dependencies are installed and install the pre-commit hook:

```bash
make install
```

## Testing

This role uses `ansible-lint` and `molecule` for testing. Run:

```bash
make test
# or
molecule test
```

Alternativly, testing can be done using [the Docker image DIND Molecule](https://gitlab.com/sjugge/docker-dind-molecule). This method is used in this projects CI as well.

#### Excluded rules

* `E403`: Package installs should not use latest. Ignore as the `yum` module does not allow for upgrading all packages without breaking this rule.
