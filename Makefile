.PHONY: install test

install:
	command -v ansible >/dev/null 2>&1 || { echo >&2 "'ansible' is required but not available, exiting."; exit 1; }
	command -v ansible-lint >/dev/null 2>&1 || { echo >&2 "'ansible-lint' is required but not available, exiting."; exit 1; }
	command -v molecule >/dev/null 2>&1 || { echo >&2 "'molecule' is required but not available, exiting."; exit 1; }
	command -v pre-commit >/dev/null 2>&1 || { echo >&2 "'pre-commit' is required but not available, exiting."; exit 1; }
	pre-commit install

test:
	molecule test