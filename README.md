# Ansible role: utilities

Ansible role to manage various miscellaneous sytem config and utilities.

* Manage users.
* Add and remove ssh keys.
* Add and remove crontab entries.
* Install and remove packages.

## Requirements

Ansible > 2.4

## Variables

Available variables are defined below, see also [`defaults/main.yml`](https://gitlab.com/sjugge/ansible-role-utilities/blob/master/defaults/main.yml). Note that additional defaults are defined in [`tasks/main.yml`](https://gitlab.com/sjugge/ansible-role-utilities/blob/master/tasks/main.yml) as well.

### Upgrade

```YAML
util_upgrade: true
```

Update all packages to the latest version. Default to `false`.

### Manage users

Managing users is possible by either `util_user_group` or `util_user_host`, the idea being that users can be defined on `group_var` and `host_var` level with one overwriting the other.
This allows specific users to be added by default to a group and more fine-grained control per host when a specific user needs access to a single or only a select few hosts.

#### Group

Specific for managin users on Ansible group_var level.

```YAML
util_user_group:
    name: foo
    home: /home/foobar
    groups: bar,baz
    append: yes
    shell: /bin/zsh
```

Manage user, their groups and shell.

#### Host

Specific for managing users on Ansible host_var level.

```YAML
util_user_host:
    name: foo
    home: /home/foobar
    groups: bar,baz
    append: yes
    shell: /bin/zsh
```

### Manage ssh keys

Manage ssh keys, either through `util_key_group` or `util_key_host`, the idea being that ssh keys can be managed both on group and host level.

#### Group

```YAML
util_key_group:
  - user: "admin"
    key: "https://keys.example.com/user"
  - user: "ci"
    key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
  - user: "foo"
    key: "{{ lookup('file', '~/.ssh/id_rsa.pub.old') }}"
    state: absent
```

Add the key(s) of https://keys.example.com/user.
Add the local file `~/.ssh/id_rsa.pub` and remove `~/.ssh/id_rsa.pub.old`.

#### Host

```YAML
util_key_host:
  - user: "admin"
    key: "https://keys.example.com/user"
  - user: "ci"
    key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
  - user: "foo"
    key: "{{ lookup('file', '~/.ssh/id_rsa.pub.old') }}"
    state: absent
```

Add the key(s) of https://keys.example.com/user.
Add the local file `~/.ssh/id_rsa.pub` and remove `~/.ssh/id_rsa.pub.old`.

### Manage packages

```YAML
util_package_install:
  - name: foo
    version: 1.0.0
```

Install the lastest version of `foo`.

```YAML
util_package_remove:
  - bar
```

Remove the package `bar`.

```YAML
apt_upgrade_choice: "safe"
```

Upgrade choice for Apt, one of dist `full`, `no`, `safe`, `yes`. Defaults to `safe`.

```YAML
util_package_purge: no
```

Apt (Debian/Ubuntu) only. Force purging of configuration files, default is `yes`.

### Manage cron

```YAML
util_cron:
  - name: backup system
    job: my-awesome-backup.sh
    user: backup
    minute: 0
    hour: 4
    weekday: 1-5
    state: present
```

Define crontab entry, it's user, command (job), scheduling and state.

## Dependencies

None.

## Example playbook

```YAML
- hosts: all
  roles:
    - sjugge.utilities
```

## Contributing & issues

See [contributing guide](https://gitlab.com/sjugge/ansible-role-utilities/blob/master/CONTRIBUTING.md).

## Copyright

[MIT License](https://gitlab.com/sjugge/ansible-role-utilities/blob/master/LICENSE)

## Author

- Jurgen Verhasselt - https://gitlab.com/sjugge
- Source: https://gitlab.com/sjugge/ansible-role-utilities
